class Countdown < Draco::Component
  attribute :remaining, default: 20 * 60
end
