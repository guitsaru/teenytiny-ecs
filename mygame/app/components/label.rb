class Label < Draco::Component
  attribute :text
  attribute :alignment_enum, default: 0
  attribute :size_enum, default: 0
end
