class Instructions < Draco::Entity
  TEXT = "Get to the red goal! Use arrow keys to move. Spacebar to teleport (use them carefully)!"

  component Label, text: TEXT, alignment_enum: 1
  component Position, x: 640, y: 710
end
