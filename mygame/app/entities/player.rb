class Player < Draco::Entity
  component PlayerControlled
  component Position, x: 50, y: 50
  component Speed, speed: 5
  component Sprite, w: 20, h: 20, path: "sprites/square-green.png"
  component Teleport, remaining: 3
end
