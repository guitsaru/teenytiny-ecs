class ScoreKeeper < Draco::Entity
  component Label, text: -> (entity) { "score: #{entity.score.score}"}, alignment_enum: 1
  component Position, x: 640, y: 650
  component Score
end
