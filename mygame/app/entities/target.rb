class Target < Draco::Entity
  component Goal
  component Position, x: 640, y: 360
  component Sprite, w: 20, h: 20, path: "sprites/square-red.png"

  def position
    @components[:position]
  end

  def sprite
    @components[:sprite]
  end
end
