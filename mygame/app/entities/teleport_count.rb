class TeleportCount < Draco::Entity
  component Label, text: -> (entity) { "teleports: #{entity.teleport.remaining}" }, alignment_enum: 1, size_enum: -2
  component PlayerControlled
  component Position, x: 60, y: 100
  component Speed, speed: 5
  component Teleport, remaining: 3
end
