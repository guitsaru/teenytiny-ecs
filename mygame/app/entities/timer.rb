class Timer < Draco::Entity
  component Countdown
  component Label, text: -> (entity) { "time left: #{entity.countdown.remaining.idiv(60) + 1}" }, alignment_enum: 1
  component Position, x: 640, y: 680
end
