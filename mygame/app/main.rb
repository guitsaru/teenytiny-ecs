def benchmark(label, &block)
  s = Time.now
  res = block.call
  et = Time.now

  timing = et - s
  puts label + ": " + (et - s).to_s + " - " + ((et - s) / (1/60) * 100).to_i.to_s
  res
end

require "app/lib/draco.rb"

require "app/components/countdown.rb"
require "app/components/goal.rb"
require "app/components/label.rb"
require "app/components/player_controlled.rb"
require "app/components/position.rb"
require "app/components/score.rb"
require "app/components/sprite.rb"
require "app/components/speed.rb"
require "app/components/teleport.rb"

require "app/entities/instructions.rb"
require "app/entities/player.rb"
require "app/entities/score_keeper.rb"
require "app/entities/target.rb"
require "app/entities/teleport_count.rb"
require "app/entities/timer.rb"

require "app/systems/check_goal.rb"
require "app/systems/player_movement.rb"
require "app/systems/render_labels.rb"
require "app/systems/render_sprites.rb"
require "app/systems/restart_game.rb"
require "app/systems/update_timer.rb"

class World < Draco::World
  entity Timer, as: :timer
  entity Player, as: :player
  entity TeleportCount
  entity Target, as: :target
  entity Instructions
  entity ScoreKeeper

  systems UpdateTimer, PlayerMovement, RenderLabels, RenderSprites, CheckGoal, RestartGame
end

def tick args
  args.state.world ||= World.new

  args.outputs.debug << args.gtk.framerate_diagnostics_primitives
  args.state.world.tick(args)
end

$gtk.reset
