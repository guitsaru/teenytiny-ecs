class CheckGoal < Draco::System
  filter PlayerControlled, Sprite

  def tick(args)
    player = world.player
    target = world.target

    player_rect = { x: player.position.x, y: player.position.y, w: player.sprite.w, h: player.sprite.h }
    target_rect = { x: target.position.x, y: target.position.y, w: target.sprite.w, h: target.sprite.h }

    if player_rect.intersect_rect?(target_rect)
      score_keeper = world.filter([Score]).first
      score_keeper.score.score += 1
      target.position.x = target.sprite.w + rand(args.grid.w - (2 * target.sprite.w))
      target.position.y = target.sprite.h + rand(args.grid.h - (2 * target.sprite.h))
    end
  end
end
