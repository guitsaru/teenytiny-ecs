class PlayerMovement < Draco::System
  filter PlayerControlled, Position, Speed

  def tick(args)
    timer = world.timer
    return if timer.countdown.remaining < 0

    entities.each { |e| move_entity(e, args) }
  end

  private
  def move_entity(entity, args)
    dir_y = 0
    dir_x = 0

    # determine the change horizontally
    if args.inputs.keyboard.up
      dir_y += entity.speed.speed
    elsif args.inputs.keyboard.down
      dir_y -= entity.speed.speed
    end

    # determine the change vertically
    if args.inputs.keyboard.left
      dir_x -= entity.speed.speed
    elsif args.inputs.keyboard.right
      dir_x += entity.speed.speed
    end

    # determine if teleport can be used
    if args.inputs.keyboard.key_down.space && entity.teleport.remaining > 0
      entity.teleport.remaining -= 1
      dir_x *= 20
      dir_y *= 20
    end

    entity.position.x += dir_x
    entity.position.y += dir_y
  end
end
