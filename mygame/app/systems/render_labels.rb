class RenderLabels < Draco::System
  filter Position, Label

  def tick(args)
    labels = entities.map do |entity|
      label = entity.label
      position = entity.position

      text = label.text
      text = text.call(entity) if text.respond_to?(:call)

      {
        x: position.x,
        y: position.y,
        text: text,
        alignment_enum: label.alignment_enum,
        size_enum: label.size_enum
      }
    end

    args.outputs.labels << labels
  end
end
