class RestartGame < Draco::System
  def tick(args)
    if args.inputs.keyboard.key_down.r
      $gtk.reset
    end
  end
end
