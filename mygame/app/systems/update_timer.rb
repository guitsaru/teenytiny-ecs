class UpdateTimer < Draco::System
  filter Countdown

  def tick(args)
    entities.each do |entity|
      if entity.countdown.remaining > -1
        entity.countdown.remaining -= 1
      else
        entity.label.text = "game over! (press r to start over)"
      end
    end
  end
end
